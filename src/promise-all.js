const fetch = require('node-fetch');

async function fetchFromGitHub(endpoint) {
  const url = `https://api.github.com${endpoint}`;
  const response = await fetch(url);
  return await response.json();
}

async function showUserAndRepos(userid) {
  // const results = await Promise.all([
  const [user, repos] = await Promise.all([
    fetchFromGitHub(`/users/${userid}`),
    fetchFromGitHub(`/users/${userid}/repos`)
  ]);

  // const user = results[0];
  // const repos = results[1];

  console.log(user.name);
  console.log(`${repos.length} repos`);
}

showUserAndRepos('sbub');
