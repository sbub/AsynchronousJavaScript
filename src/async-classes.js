const fetch = require('node-fetch');

class GitHubApiClient {
  async fetchGitHubUser(userid) {
    let url = `https://api.github.com/users/${userid}`;
    let response = await fetch(url);
    return await response.json();
  }
}

(async function() {
  const client = new GitHubApiClient();
  const user = await client.fetchGitHubUser('sbub');
  console.log(user.name);
  console.log(user.location);
}())
