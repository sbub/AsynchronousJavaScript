const fetch = require('node-fetch');

/* Video 1 */
/* Video 1 - Write an async function with async/await */
/* uncomment to use without async/await syntax */
// function showGitHubUser(userid) {
//   const url = `https://api.github.com/users/${userid}`;
//   fetch(url)
//     .then(response => response.json())
//     .then(user => {
//       console.log(user.name);
//       console.log(user.location);
//     })
// }

/* Video 1 */
/* Video 1 - Write an async function with async/await */
// async function showGitHubUser(userid) {
//   const url = `https://api.github.com/users/${userid}`;
//   const response = await fetch(url);
//   const user = await response.json();
//   console.log(user.name);
//   console.log(user.location);
// }

/* Video 2 */
/* Video 2 - Call an async function in a promise-chain */
async function showGitHubUser(userid) {
  const url = `https://api.github.com/users/${userid}`;
  const response = await fetch(url);
  return await response.json();
}

showGitHubUser('sbub')
  .then(user => {
    console.log(user.name);
    console.log(user.location);
  })

//because fn showGitHubUser returns Promise - it need to be resolved
//below will not work
// var user = showGitHubUser('sbub');
