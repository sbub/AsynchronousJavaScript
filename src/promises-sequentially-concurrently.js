const fetch = require('node-fetch');

async function fetchFromGitHub(endpoint) {
  const url = `https://api.github.com${endpoint}`;
  const response = await fetch(url);
  return await response.json();
}

async function showUserAndRepos(userid) {

  //if you want to run it one after the other,
  //the use await: you will pause at line 13 until
  //it will be resolved
  // const user = await fetchFromGitHub(`/users/${userid}`);
  // const repos = await fetchFromGitHub(`/users/${userid}/repos`);

  //this is done to run concurrent requests;
  const userPromise = fetchFromGitHub(`/users/${userid}`);
  const repoPromise = fetchFromGitHub(`/users/${userid}/repos`);

  const user = await userPromise;
  const repos = await repoPromise;

  console.log(user.name);
  console.log(`${repos.length} repos`);
}

showUserAndRepos('sbub');


console.log('memory usage', process.memoryUsage().heapUsed);
