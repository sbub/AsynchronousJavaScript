const fetch = require('node-fetch');

async function showGitHubUser(userid) {
  const url = `https://api.github.com/users/${userid}`;
  const response = await fetch(url);

  let body = await response.json();

  if(response.status !== 200) {
    throw Error(body.message);
  }

  return body;
}

showGitHubUser('sbub')
  .then(user => {
    console.log(user.name);
  })
  .catch((err) => {
    console.log('Error:', err.message);
  })
