const fetch = require('node-fetch');

async function fetchGitHubUser(userid) {
  const url = `https://api.github.com/users/${userid}`;
  const response = await fetch(url);

  let body = await response.json();

  if(response.status !== 200) {
    throw Error(body.message);
  }

  return body;
}

async function showGitHubUser(userid) {
  try {
    const user = await fetchGitHubUser(userid);
    console.log(user.name);
    console.log(user.location);
  }
  catch(err) {
    console.log('Error:', err.message);
  }
}

showGitHubUser('sbubs');
