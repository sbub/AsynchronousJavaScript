const fetch = require('node-fetch');

// var showGitHubUser = async function (userid) {
//arrow type syntax
var fetchGitHubUser = async (userid) => {
  let url = `https://api.github.com/users/${userid}`;
  let response = await fetch(url);
  return await response.json();
}

(async function() {
  const user = await fetchGitHubUser('sbub')
  console.log(user.name);
  console.log(user.location);
}())
