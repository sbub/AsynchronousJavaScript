const Bluebird = require('bluebird');

async function main() {
  // const x = await 43;
  // console.log('x', x);
  console.log('Working ...');
  await Bluebird.delay(2000);
  console.log('Done.');
}

main();
